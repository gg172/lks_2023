<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contoh;

class ContohController extends Controller
{
    public function index(){
        $contoh = Contoh::all();
        
        return view('test.index', ['contoh' => $contoh]);
    }
}
