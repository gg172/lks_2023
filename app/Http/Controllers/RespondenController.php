<?php

namespace App\Http\Controllers;

use App\Models\Responden;
use Illuminate\Http\Request;

class RespondenController extends Controller
{
    public function index(){
        $responden = Responden::all();
        return view('kuesioner.responden', ['responden' => $responden]);
    }
}
