<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Survei;

class SurveiController extends Controller
{
    public function index(){
        $survei = Survei::all();
        return view('kuesioner.survei', ['survei' => $survei]);
    }
    public function jawab(){
         $survei = Survei::all();   

   
        return view('kuesioner.jawab', ['survei' => $survei]);
    }

    public function tambah()
    {
       
        return view('kuesioner.survei-tambah');
    }

    public function simpan(Request $request)
    {
        $id = new Survei;
        $id = $id->max('id') + 1;
        Survei::create([
            'id' => $id,
            'judul' => $request->judul,
            'periode_awal' => $request->periode_awal,
            'periode_akhir' => $request->periode_akhir,
        ]);
        return redirect()->route('survei');
    }

    public function edit($id)
    {
        // dd($id);
        $survei = Survei::find($id);
        return view('kuesioner.survei-edit', ['survei' => $survei]);
    }

    public function update(Request $request)
    {
        $survei = Survei::find($request->id);
        // $id_buku = $id_buku->max('id_buku') + 1;
        $id = $survei->id;

        $survei->judul = $request->judul;
        $survei->periode_awal = $request->periode_awal;
        $survei->periode_akhir = $request->periode_akhir;
       

        
        $survei->save();
        return redirect()->route('survei');
    }

    public function hapus($id)
    {
        //dd($id_buku);
        $survei = Survei::findorfail($id);
        
        
        $survei->delete();
        return redirect(route('survei'));
    }
}
