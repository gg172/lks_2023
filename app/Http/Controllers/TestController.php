<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rating;

class TestController extends Controller
{
    //
    public function index()
    {
        $data = [
            "title" => "test",
        ];

        return view('test.index', $data);
    }
    public function index2(){
        $rating = Rating::all();
        // dd($rating);
        return view('test.rating', ['rating' => $rating ]);
    }

    public function store(Request $request)
    {
        $id = new Rating;
        $id = $id->max('id') + 1;
        Rating::create([
            'id' => $id,
            'nama' => $request->nama,
            'no_telp' => $request->no_telp,
            'alamat'=> $request->alamat,
            'rating' => $request->rating,
            'rating_2' => $request->rating_2,
            'rating_3' => $request->rating_3,
            'short_answer' => $request->short_answer,
            
        ]);
        return redirect()->route('admin.selesai');
    }
    public function hapus($id)
    {
        //dd($id_buku);
        $rating = Rating::findorfail($id);
        
  
        $rating->delete();
        return redirect(route('admin.test.rating'));
    }
    public function done(){
        return view('pertanyaan.next');
    }
}