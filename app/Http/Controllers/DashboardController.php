<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class DashboardController extends Controller
{
    public function index (){
        
        $id_user = auth()->user()->id_user;
        
        $data = User::find($id_user);
        // dd($data->username);
        return view('dashboard.dashboard', ['pengguna' => $data]);

    }

    public function saran(){
        return view('dashboard.saran');
    }
}
