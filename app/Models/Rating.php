<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory;

    protected $tabel = "ratings";
    protected $primaryKey = 'id';
    protected $fillable = ['nama','no_telp','alamat' ,'rating','rating_2','rating_3','short_answer'];
}
