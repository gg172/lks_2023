<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survei extends Model
{
    use HasFactory;

    protected $table = "tb_surveis";
    protected $primarykey = "id";
    protected $fillable = ['judul','periode_awal','periode_akhir'];
}
