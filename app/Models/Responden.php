<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Responden extends Model
{
    use HasFactory;
    protected $table = "tb_survei_respondens";
    protected $primarykey = "id";
    protected $fillable = ['id_survei','nama','no_telp','alamat'];
}
