<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    use HasFactory;
    protected $table = "tb_survei_pertanyaans";
    protected $primarykey = "id";
    protected $fillable = ['id_survei','pertanyaan','keterangan','urut'];
}
