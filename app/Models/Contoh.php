<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contoh extends Model
{
    use HasFactory;
    protected $table = "tb_contoh_pertanyaans";
    protected $primarykey = "id";
    protected $fillable = ['id','pertanyaan'];
}
