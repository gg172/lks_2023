<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    use HasFactory;

    protected $tabel = "jabatan";

    protected $primaryKey = 'id';

    protected $fillable = ['jabatan'];
}