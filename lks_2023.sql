-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2023 at 05:55 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lks_2023`
--

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` int(11) NOT NULL,
  `jabatan` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `jabatans`
--

INSERT INTO `jabatans` (`id`, `jabatan`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `rating` int(25) NOT NULL,
  `rating_2` int(4) NOT NULL,
  `rating_3` int(4) NOT NULL,
  `short_answer` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `nama`, `no_telp`, `alamat`, `rating`, `rating_2`, `rating_3`, `short_answer`, `created_at`, `updated_at`) VALUES
(14, 'Fahri Azis Purnomo', '08563829872', 'gondokusuman, yogyakarta', 5, 2, 1, '', '2023-05-23 18:14:36', '2023-05-23 18:14:36'),
(15, 'fahry', '45687896', 'gondokusuman, yogyakarta', 4, 3, 2, 'ada lahh', '2023-05-23 20:03:31', '2023-05-23 20:03:31'),
(16, 'Fahri Aja', '0856013418268', 'kuwon', 5, 5, 5, 'Apa ya', '2023-05-23 20:11:10', '2023-05-23 20:11:10'),
(17, 'adminn', '0856013418268', 'gondokusuman, yogyakarta', 5, 5, 5, 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius dolore, ab tenetur aspernatur esse laudantium! Excepturi suscipit odio corporis tempore itaque inventore, quasi quos incidunt, explicabo nihil repellendus alias eius numquam quaerat veniam id ad. Adipisci fugiat itaque libero qui!', '2023-05-23 20:11:57', '2023-05-23 20:11:57'),
(18, 'fahry', '0856013418268', 'gondokusuman, yogyakarta', 5, 5, 5, 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam quis error sed harum et vitae. Sint eveniet sunt impedit aspernatur iste, dicta accusantium unde nam eligendi quis ut et non dignissimos consectetur praesentium a deleniti tempora, totam itaque? Eligendi maiores, doloremque est dolorem, minus nisi, corrupti dignissimos veniam quisquam tempora alias ut.', '2023-05-23 20:13:18', '2023-05-23 20:13:18'),
(19, 'adminn', '0856013418268', 'gondokusuman, yogyakarta', 5, 4, 3, 'Belum Jadi', '2023-05-23 20:53:09', '2023-05-23 20:53:09');

-- --------------------------------------------------------

--
-- Table structure for table `tb_contoh_pertanyaans`
--

CREATE TABLE `tb_contoh_pertanyaans` (
  `id` int(11) NOT NULL,
  `pertanyaan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_contoh_pertanyaans`
--

INSERT INTO `tb_contoh_pertanyaans` (`id`, `pertanyaan`) VALUES
(1, 'Apakah kamu puas dengan pelayanan disini?'),
(2, 'Apakah Anda Puas Dengan Kebersihan Ruangan Kami?'),
(3, 'Apakah Anda Merasa Nyaman Disetiap Ruangan Kami?');

-- --------------------------------------------------------

--
-- Table structure for table `tb_surveis`
--

CREATE TABLE `tb_surveis` (
  `id` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `periode_awal` timestamp NOT NULL DEFAULT current_timestamp(),
  `periode_akhir` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_surveis`
--

INSERT INTO `tb_surveis` (`id`, `judul`, `periode_awal`, `periode_akhir`, `created_at`, `updated_at`) VALUES
(1, 'Survei Restoran Brillyan', '2023-05-22 17:00:00', '2023-05-28 17:00:00', '2023-05-23 03:40:25', '2023-05-23 00:51:37');

-- --------------------------------------------------------

--
-- Table structure for table `tb_survei_jawabans`
--

CREATE TABLE `tb_survei_jawabans` (
  `id` int(11) NOT NULL,
  `id_survei` int(11) NOT NULL,
  `id_survei_responden` int(11) NOT NULL,
  `id_survei_pertanyaan` int(11) NOT NULL,
  `jawaban` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_survei_jawabans`
--

INSERT INTO `tb_survei_jawabans` (`id`, `id_survei`, `id_survei_responden`, `id_survei_pertanyaan`, `jawaban`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '5', '2023-05-23 04:45:40', '2023-05-23 04:45:40');

-- --------------------------------------------------------

--
-- Table structure for table `tb_survei_pertanyaans`
--

CREATE TABLE `tb_survei_pertanyaans` (
  `id` int(11) NOT NULL,
  `id_survei` int(11) NOT NULL,
  `pertanyaan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `urut` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_survei_pertanyaans`
--

INSERT INTO `tb_survei_pertanyaans` (`id`, `id_survei`, `pertanyaan`, `keterangan`, `created_at`, `updated_at`, `urut`) VALUES
(1, 1, 'Apakah kamu puas dengan pelayanan disini?', '5', '2023-05-23 02:17:26', '2023-05-23 02:17:26', 1),
(2, 1, 'Apakah Anda Puas Dengan Kebersihan Ruangan Kami?', '5', '2023-05-24 02:01:22', '2023-05-24 02:01:22', 2),
(3, 1, 'Apakah Anda Merasa Nyaman Disetiap Ruangan Kami?', '5', '2023-05-24 02:01:56', '2023-05-24 02:01:56', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tb_survei_respondens`
--

CREATE TABLE `tb_survei_respondens` (
  `id` int(11) NOT NULL,
  `id_survei` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_survei_respondens`
--

INSERT INTO `tb_survei_respondens` (`id`, `id_survei`, `nama`, `no_telp`, `alamat`, `created_at`, `updated_at`) VALUES
(1, 1, 'Fahri', '088233082908', 'Kuwon', '2023-05-23 02:25:21', '2023-05-23 02:25:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `jabatan` int(11) NOT NULL,
  `password` text NOT NULL,
  `foto` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama`, `username`, `jabatan`, `password`, `foto`, `created_at`, `updated_at`) VALUES
(4, 'Fahri Azis P', 'admin', 1, '$2y$10$nKl6JzXHEnw6K/Fjj/A83unmEh693wferi20GhegGBgMRwQAHNive', 'DSCF8343.JPG', '2023-05-22 19:23:01', '2023-05-23 18:50:27'),
(6, 'User', 'fahry', 2, '$2y$10$wDl3xImadE34gd.E69t0KO2FEbXzEbCPLvdqIH0PjfBqzwZ.YsJvO', 'DSCF8372.JPG', '2023-05-22 23:18:50', '2023-05-23 18:52:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_contoh_pertanyaans`
--
ALTER TABLE `tb_contoh_pertanyaans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_surveis`
--
ALTER TABLE `tb_surveis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_survei_jawabans`
--
ALTER TABLE `tb_survei_jawabans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_survei_pertanyaans`
--
ALTER TABLE `tb_survei_pertanyaans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_survei_respondens`
--
ALTER TABLE `tb_survei_respondens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tb_contoh_pertanyaans`
--
ALTER TABLE `tb_contoh_pertanyaans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_surveis`
--
ALTER TABLE `tb_surveis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_survei_jawabans`
--
ALTER TABLE `tb_survei_jawabans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_survei_pertanyaans`
--
ALTER TABLE `tb_survei_pertanyaans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_survei_respondens`
--
ALTER TABLE `tb_survei_respondens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
