@extends('template.admin')
@section('konten')
    
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.css" media="all" rel="stylesheet" type="text/css" />



    <div class="row mt-5 mb-5">
        <div class="col-12">
            <div class="card p-3">
                <div class="card-title ">
                    <h3 class="text-center text-body-secondary mt-2">Survei Kepuasan</h3>
                    <p class="ps-3">Survei Kepuasan Rumah Sakit UII</p>
                    <p class="ps-3 fst-italic fw-lighter">Periode</p>
                    <p class="ps-3 fst-italic fw-lighter"> 2023-05-23 00:00:00 - 2023-05-29 00:00:00</p>
                </div>
              
                <div class="card-body">
                    <form action="{{ route('admin.test.store') }}" method="post">
                    {{ csrf_field() }}

                        <label for="">Nama</label>
                        <input id="input-id"  name="nama" type="Text" class="form-control" min=0 max=5> 
                        <label for="">No Telp</label>
                        <input id="input-id"  name="no_telp" type="Text" class="form-control" > 
                        <label for="">Alamat</label>
                        <input id="input-id"  name="alamat" type="Text" class="form-control"> 
                        <br>
                        <br>
                        <hr>
                        <h5 class="mb-3 text-body-secondary">Pertanyaan :</h5>
                        <label for="">1. Apakah kamu puas dengan pelayanan disini?</label>
                        <input id="input-id" name="rating" type="number" class="rating" min=0 max=5> 

                        <label for="">2. Apakah Anda Puas Dengan Kebersihan Ruangan Kami?</label>
                        <input id="input-id" name="rating_2" type="number" class="rating" min=0 max=5> 

                        <label for="">3. Apakah Anda Merasa Nyaman Disetiap Ruangan Kami?</label>
                        <input id="input-id" name="rating_3" type="number" class="rating" min=0 max=5> 

                        <label for="">4. Berikan Saran Dan Kritik Anda!</label>
                        <input name="short_answer" class="form-control mt-2" type="text" id="pass" placeholder="Masukan Jawaban Anda">

                        <button type="submit" class="btn btn-success mt-3">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/js/star-rating.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

    @endsection


   