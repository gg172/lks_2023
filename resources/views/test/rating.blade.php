@extends('template.admin')
@section('konten')



<div class="text-center ">
    <h3 class="fw-bold  text-body-secondary  mb-5">Contoh Data Responden</h3>
</div>
<div class="col-12 mb-5">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-survey ">
                    <thead>
                        <tr class="text-center">
                            <th>id</th>
                            <th>Nama</th>
                            <th>No Telp</th>
                            <th>Alamat</th>
                            <th>Pertanyaan 1</th>
                            <th>Pertanyaan 2</th>
                            <th>Pertanyaan 3</th>
                            <th>Saran</th>
                            <th>Created At</th>
                            
                            @can('isAdmin')
                            <th>Aksi</th>
                            @endcan
                        </tr>
                    </thead>
                    @foreach($rating as $u)
                    <tbody>
                        <td>{{ $u->id }} </td>
                        <td>{{ $u->nama }} </td>
                        <td>{{ $u->no_telp }} </td>
                        <td>{{ $u->alamat }} </td>
                        <td>{{ $u->rating }} </td>
                        <td>{{ $u->rating_2 }} </td>
                        <td>{{ $u->rating_3 }} </td>
                        <td>{{ $u->short_answer }} </td>
                        <td>{{ $u->created_at }}</td>
                        @can('isAdmin')
                        <td>
                            <a href="{{ route('admin.test.hapus', $u->id) }}" class="btn btn-danger"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z" />
                                    <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z" />
                                </svg> Hapus</a>
                        </td>
                        @endcan
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

