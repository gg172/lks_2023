@extends('template.admin')
@section('konten')

<div class="container mt-5">
    <div class="col-md-12">
        <form action="{{ route('user.password') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="password">Password Baru</label>
                <input type="hidden" name="id_user" value="{{ $data->id_user }}">
                <input type="text" name="password" class="form-control" id="nama" placeholder=" masukan password" required=''>
            </div>
            
            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
    </div>
</div>

@endsection