@extends('template.admin')
@section('konten')

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-survey">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>id survei</th>
                            <th>Nama</th>
                            <th>No Telp</th>
                            <th>Alamat</th>
                            
                        </tr>
                    </thead>
                    @foreach($responden as $u)
                    <tbody>
                        <td>{{ $u->id }} </td>
                        <td>{{ $u->id_survei }} </td>
                        <td>{{ $u->nama }} </td>
                        <td>{{ $u->no_telp }} </td>
                        <td>{{ $u->alamat }} </td>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>



@endsection