
@extends('template.admin')
@section('konten')
<br>
<br>
<br>
    <div class="">
        <div class="card">
            <div class="p-3 col-md-12">
                <h4 class="text-center fw-lighter">Buat Survei</h4>
                <form action="{{ route('survei.simpan') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="">Judul</label>
                        <input type="text" class="form-control" name="judul" required>
                    </div>
                    <div class="form-group">
                        <label for="">Periode awal</label>
                        <input type="date" class="form-control" name="periode_awal" required>
                    </div>
                   
                    <div class="form-group">
                        <label for="">Periode akhir</label>
                        <input type="date" class="form-control" name="periode_akhir" required>
                    </div>
                    <input class="btn btn-info mt-3" type="submit" value="simpan">
                </form>
                <hr>
            <p>PERTANYAAN</p>

            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.3.js"></script>

@endsection