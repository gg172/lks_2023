<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  @foreach($survei as $u)
  <title>{{ $u->judul }} </title>
  <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.css" media="all" rel="stylesheet" type="text/css" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
</head>

<body>

  <div class="container mt-5">
    <div class="card bg-light p-2">
      
      <h4 class="text-center fw-bold ">Survei Kepuasan</h4>
      <h5 class="fw-bold">{{ $u->judul }}</h5>
      <p>periode</p>
      <p>{{ $u->periode_awal }} - {{ $u->periode_akhir }}</p>

      @endforeach
      <hr>
      <label for="">Nama Lengkap</label>
      <input class="form-control mt-1" type="text" name="" id="">
      <label for="" class="mt-2">Nomor Telephone</label>
      <input class="form-control mt-1" type="text" name="" id="">
      <label for="" class="mt-2">Alamat</label>
      <input class="form-control mt-1" type="text" name="" id="">

      <hr>
      <p class="fw-bold">Pertanyaan</p>

      <label for="">1. Apakah kamu puas dengan pelayanan disini?</label>
      <input id="input-id" name="rating" type="number" class="rating" min=0 max=5>

      <label for="">2. Apakah Anda Puas Dengan Kebersihan Ruangan Kami?</label>
      <input id="input-id" name="rating_2" type="number" class="rating" min=0 max=5>

      <label for="">3. Apakah Anda Merasa Nyaman Disetiap Ruangan Kami?</label>
      <input id="input-id" name="rating_3" type="number" class="rating" min=0 max=5>

      <button type="submit" class="btn btn-success mt-3">Kirim</button>


    </div>
  </div>
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0  1440 325">
    <path fill="#0099ff" fill-opacity="0.65" d="M0,64L48,96C96,128,192,192,288,213.3C384,235,480,213,576,186.7C672,160,768,128,864,144C960,160,1056,224,1152,218.7C1248,213,1344,139,1392,101.3L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
  </svg>
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/js/star-rating.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</body>

</html>