@extends('template.admin')
@section('konten')

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-survey">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>id survei</th>
                            <th>Pertanyaan</th>
                            <th>Keterangan</th>
                            
                        </tr>
                    </thead>
                    @foreach($pertanyaan as $u)
                    <tbody>
                        <td>{{ $u->id }} </td>
                        <td>{{ $u->id_survei }} </td>
                        <td>{{ $u->pertanyaan }} </td>
                        <td>{{ $u->keterangan }} </td>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>



@endsection