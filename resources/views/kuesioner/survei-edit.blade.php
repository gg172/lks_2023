@extends('template.admin')
@section('konten')

<div class="container mt-5">
    <div class="col-md-12">
        <form action="{{ route('survei.update') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="nama">Judul</label>
                <input type="hidden" name="id" value="{{ $survei->id }}">
                <input type="text" name="judul" class="form-control" id="judul" value="{{ $survei->judul }}" placeholder="Masukan Judul" required=''>
            </div>
            <div class="form-group">
                <label for="username">Periode Awal</label>
                <input type="date" name="periode_awal" class="form-control" value="{{ $survei->periode_awal }}" aria-describedby="" placeholder="" required=''>
            </div>
            <div class="form-group">
                <label for="username">Periode Akhir</label>
                <input type="date" name="periode_akhir" class="form-control" value="{{ $survei->periode_akhir }}" aria-describedby="" placeholder="" required=''>
            </div>
         
            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
    </div>
</div>

@endsection