<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Modernize Free</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/png" href="../assets/images/logos/favicon.png" />
    <link rel="stylesheet" href="../assets/css/styles.min.css" />
    <link rel="stylesheet" href="{{ asset('assets/css/styles.min.css') }}" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
</head>

<body>


<div class="container">

    <div class="row mt-5">
        <div class="col-12">
        
            <div class="card">
            <div class="card-title d-flex justify-content-center mt-5 mb-5"> 
                <img src="{{ asset('assets/images/logos/logo-head.png') }}" class="img-fluid" style="max-width: 20%;" alt="dsfs"> 
            </div>
                <div class="card-body">
                    
                    <h2 class="text-center fw-bold">Survei Perpustakaan Labdagati SMK N1 Pundong</h2>
                    <form action="{{ route('admin.test.store') }}" method="post">
                        
                        {{ csrf_field() }}

                        <label for="">Nama</label>
                        <input id="input-id" name="nama" type="Text" class="form-control " style="width:50%;" id="exampleInputEmail1" aria-describedby="" required="" placeholder="Masukan Nama Anda" require>
                        <br>
                        <br>

                        <label for="">1. Bagaimana pendapat Saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayananya?</label>
                        <input id="input-id" name="rating" type="number" class="rating" min=0 max=5>

                        <label for="">2. Bagaimana pemahaman Saudara tentang kemudahan prosedur pelayanan di perpustakaan ?</label>
                        <input id="input-id" name="rating_2" type="number" class="rating" min=0 max=5>

                        <label for="">Bagaimana pendapat Saudara tentang kecepatan waktu dalam memberikan pelayanan ?</label>
                        <input id="input-id" name="rating_3" type="number" class="rating" min=0 max=5>

                        <button type="submit" class="btn btn-success">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/js/star-rating.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <script src="../assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="../assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/js/sidebarmenu.js"></script>
    <script src="../assets/js/app.min.js"></script>
    <script src="../assets/libs/apexcharts/dist/apexcharts.min.js"></script>
    <script src="../assets/libs/simplebar/dist/simplebar.js"></script>
    <script src="../assets/js/dashboard.js"></script>
</body>

</html>