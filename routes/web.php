<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');
Route::get('/selesai', 'App\Http\Controllers\TestController@done')->name('admin.selesai');

Route::get('jawab', 'App\Http\Controllers\SurveiController@jawab')->name('jawab');

Route::get('login', 'App\Http\Controllers\LoginController@index')->name('login');
Route::get('logout', 'App\Http\Controllers\LoginController@logout')->name('logout');
Route::post('proseslogin', 'App\Http\Controllers\LoginController@proseslogin')->name('proseslogin');

Route::get('pertanyaan', 'App\Http\Controllers\PertanyaanController@index')->name('pertanyaan');
Route::get('responden', 'App\Http\Controllers\RespondenController@index')->name('responden');

Route::group(['middleware' => 'App\Http\Middleware\ceklogin'], function () {
    Route::get('/admin/test', 'App\Http\Controllers\TestController@index')->name('admin.test');
    Route::get('/admin/test/rating', 'App\Http\Controllers\TestController@index2')->name('admin.test.rating');
    Route::post('/admin/test/store', 'App\Http\Controllers\TestController@store')->name('admin.test.store');

    Route::get('survei', 'App\Http\Controllers\SurveiController@index')->name('survei');
    Route::get('survei/tambah', 'App\Http\Controllers\SurveiController@tambah')->name('survei.tambah');
    Route::post('survei/simpan', 'App\Http\Controllers\SurveiController@simpan')->name('survei.simpan');
    Route::get('survei/edit/{id}', 'App\Http\Controllers\SurveiController@edit')->name('survei.edit');
    Route::post('survei/update', 'App\Http\Controllers\SurveiController@update')->name('survei.update');
    Route::get('survei/hapus/{id}', 'App\Http\Controllers\SurveiController@hapus')->name('survei.hapus');
    Route::get('saran', 'App\Http\Controllers\DashboardController@saran')->name('saran');

    Route::middleware(['App\Http\Middleware\role:isAdmin'])->group(function () {

        Route::get('user', 'App\Http\Controllers\PenggunaController@index')->name('user');
        Route::get('user/tambah', 'App\Http\Controllers\PenggunaController@tambah')->name('user.tambah');
        Route::post('user/simpan', 'App\Http\Controllers\PenggunaController@simpan')->name('user.simpan');
        Route::post('user/test_ajax', 'App\Http\Controllers\PenggunaController@test_ajax')->name('user.test_ajax');
        Route::post('user/update', 'App\Http\Controllers\PenggunaController@update')->name('user.update');
        Route::get('user/edit/{id}', 'App\Http\Controllers\PenggunaController@edit')->name('user.edit');
        Route::post('user/password', 'App\Http\Controllers\PenggunaController@password')->name('user.password');
        Route::get('user/ubah/{id}', 'App\Http\Controllers\PenggunaController@ubah')->name('user.ubah');
        Route::get('user/hapus/{id}', 'App\Http\Controllers\PenggunaController@hapus')->name('user.hapus');

        

        Route::get('admin/test/hapus/{id}', 'App\Http\Controllers\TestController@hapus')->name('admin.test.hapus');
    });
});
